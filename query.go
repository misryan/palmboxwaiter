package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func query(qstr string) (rt string) {
	url := "https://beta-s.cs57.force.com/palmbox/services/apexrest/PB/InfoQuery/OP?q=" + qstr
	fmt.Println(url)

	spaceClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "spacecount-tutorial")

	res, getErr := spaceClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	fmt.Println(string(body)) //print Request.Body

	var OPs []OP
	jsonErr := json.Unmarshal(body, &OPs)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	//fmt.Fprintf(w, "%+v", OPs)
	fmt.Println(OPs)
	rt = OPs[0].Place_Ezmem_Name__c
	return
	// for i := 0; i < len(OPs[0].PB_OperationPoint_0vuZ__r.Records); i++ {
	// 	fmt.Fprintf(w, "Asset:%s\n", OPs[0].PB_OperationPoint_0vuZ__r.Records[i].Name)
	// }

}

type OP struct {
	Place_Ezmem_Name__c       string
	OPStatus__c               string
	PB_OperationPoint_0vuZ__r AssetList
}

type AssetList struct {
	Records []Asset `json:"records"`
}

type Asset struct {
	Id           string
	Name         string
	Position__c  string
	CabinType__c string
}

// func main() {
// 	http.HandleFunc("/query", query)
// 	err := http.ListenAndServe(":9090", nil)
// 	if err != nil {
// 		log.Fatal("ListenAndServe: ", err)
// 	}
// }
